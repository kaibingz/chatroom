
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.Socket;


/**
 * Created by zhangkaibing on 3/10/15.
 */

//This thread used by P2P server to receive messages.
public class ClientP2P implements Runnable {

    private Socket partnerSocket;
    private BufferedReader in;
    private String message = "";

    public ClientP2P (Socket s) {
        partnerSocket = s;
    }

    public void run() {
        try {
            try {
                in = new BufferedReader(new InputStreamReader(partnerSocket.getInputStream()));

                while ((message = in.readLine()) != null) {
                    if (message.equals("#BACK")) {//If message==BACK, set the P2P server flag to false, then the main thread capture this value change and back to the chatting room mode.
                        ChatClient.protectedStandardOutput("System Info: P2P Chat Ended. Back to Chatting Room");
                        ChatClient.protectedSetServerFlag(false);

                        break;
                    } else if (message.equals("#DECLINE")) {//The P2P connection request is declined, then quit to the chatting room.
                        ChatClient.protectedStandardOutput("P2P invitation declined.");
                        ChatClient.protectedSetServerFlag(false);
                        break;
                    } else {
                        ChatClient.protectedStandardOutput(message);//Push normal chatting messages.
                    }

                }

            } finally {
                partnerSocket.close();//Close socket after finished P2P chatting.
            }
        } catch (Exception e) {
            System.out.println(e);
        }

    }

}
