/**
 * Created by zhangkaibing on 3/13/15.
 */
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;
// This class is for login, to verify clients.
public class ClientVerify {
    public ClientVerify (Socket s) {
        serverSocket = s;
    }
    public String Username="";
    private Socket serverSocket;
    private BufferedReader in;
    private String message = "";

    public boolean Verified(){
        try {
            boolean Trial=true;//If Trial==false : Server don't allow to try again
            try {
                in = new BufferedReader(new InputStreamReader(serverSocket.getInputStream()));
                PrintWriter out = new PrintWriter(serverSocket.getOutputStream(),true);
                while (Trial) {
                    if ((message = in.readLine()) != null) {
                        if (message.equals("###CONNECTED###")){
                            ChatClient.protectedStandardOutput("Server Connection Established!");
                        } else if (message.equals("###USERNAME###")){
                            ChatClient.protectedStandardOutput("Please Input User Name");
                            Scanner sca = new Scanner(System.in);
                            String send = sca.next();
                            out.println(send);
                            Username=send;
                        } else if (message.equals("###PASSWORD###")){
                            ChatClient.protectedStandardOutput("Please Input Password");
                            Scanner sca = new Scanner(System.in);
                            String send = sca.next();
                            out.println(send);
                        } else if (message.equals("###SUCCEED###")){
                            ChatClient.protectedStandardOutput("Login Succeed!");
                            return true;
                        } else if (message.equals("###NOTEXIST###")) {
                            ChatClient.protectedStandardOutput("User Not Exist, Please Try Again.");
                        } else if (message.equals("###PWDNOTMATCH###")) {
                            ChatClient.protectedStandardOutput("Username and password don't match. Pleas try again.");
                        } else if (message.equals("###BLOCKED###")){
                            ChatClient.protectedStandardOutput("User blocked. Please try later.");
                            Trial=false;
                        } else if (message.equals("###PARTNER###")){
                            ChatClient.protectedStandardOutput("Please Input The Partner Name");
                            Scanner sca = new Scanner(System.in);
                            String send = sca.next();
                            out.println(send);
                        } else {
                            ChatClient.protectedStandardOutput(message);
                        }
                    }
                }
            } finally {
                if (!Trial) {
                    serverSocket.close();
                }
            }
        }
        catch (Exception e) {
            System.out.println(e);
        }
        return false;
    }
}
