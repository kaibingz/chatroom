
import java.io.PrintWriter;
import java.net.Socket;
import java.util.concurrent.TimeUnit;

/**
 * Created by zhangkaibing on 3/10/15.
 */

//Send Heartbeat every 5 seconds.
public class ClientHeartBeat implements Runnable {

    private Socket serverSocket;

    public ClientHeartBeat (Socket s) {
        serverSocket = s;
    }

    public void run() {
        try {
            try {
                PrintWriter out = new PrintWriter(serverSocket.getOutputStream(),true);
                while (true) {
                    out.println("###HEARTBEAT###");
                    TimeUnit.SECONDS.sleep(5);
                }
            }
            finally {
                serverSocket.close();
            }
        }
        catch (Exception e) {
            System.out.println(e);
        }

    }

}
