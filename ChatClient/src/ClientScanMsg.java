

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.concurrent.TimeUnit;

/**
 * Created by zhangkaibing on 3/10/15.
 */

//This thread is used to scan message from server
public class ClientScanMsg implements Runnable {

    private Socket serverSocket;
    private Socket partnerSocket;
    private BufferedReader P2Pin;
    private BufferedReader in;
    private String message = "";

    public ClientScanMsg (Socket s) {
        serverSocket = s;
    }

    public void run() {
        try {
            try {
                in = new BufferedReader(new InputStreamReader(serverSocket.getInputStream()));
                while (true) {
                    message = in.readLine();
                    if (message != null) {
                        if (message.equals("###PARTNER###")) {
                            ChatClient.protectedStandardOutput("Partner's name:");
                        } else if (message.equals("###OFFLINE###")) {
                            ChatClient.protectedStandardOutput("User Offline");
                        } else if (message.equals("###NOTEXIST###")) {
                            ChatClient.protectedStandardOutput("Inexistent user.");
                        } else if (message.equals("###BLOCKSUCCEED###")) {
                            ChatClient.protectedStandardOutput("Block succeed.");
                        } else if (message.equals("###UNBLOCKSUCCEED###")) {
                            ChatClient.protectedStandardOutput("Unblock succeed.");
                        }
                        /*
                        else if (message.equals("###UNBLACKWHOM###")){
                            ChatClient.protectedStandardOutput("Whom do you want to remove from blacklisting?");
                        }
                        */
                        else if (message.equals("###BLOCKED###")){
                            ChatClient.protectedStandardOutput("You are blocked by that user. Unable to connect.");
                        } else if (message.length() >= 14 && message.substring(0, 14).equals("###P2PSETUP###")) {
                            int portNumber = Integer.parseInt(message.substring(14));
                            ChatClient.requestP2P(portNumber);//Let this client be server of P2P chat.
                            while (ChatClient.readServerFlag()) {//When acting as P2P server, repduce the pressure of listening to Server.
                                TimeUnit.SECONDS.sleep(1);
                            }
                        } else if (message.length()>=15 && message.substring(0, 15).equals("###P2PINVITE###")) {//If being invited to join P2P
                            //Get information about invitor
                            String p2pmsg_1 = message.substring(15);
                            int partition_1 = p2pmsg_1.indexOf("#");
                            String invitingUserName = p2pmsg_1.substring(0, partition_1);
                            String p2pmsg_2 = p2pmsg_1.substring(partition_1 + 3);
                            int partition_2 = p2pmsg_2.indexOf("#");
                            ChatClient.P2PIP = p2pmsg_2.substring(0, partition_2);
                            ChatClient.P2PportNumber = Integer.parseInt(p2pmsg_2.substring(partition_2 + 3));
                            ChatClient.protectedStandardOutput("P2P Connection Invitation From: " + invitingUserName);
                            ChatClient.protectedStandardOutput("The Invitor's IP is: " + ChatClient.P2PIP);
                            ChatClient.protectedStandardOutput("The invitor's portnumber is: " + ChatClient.P2PportNumber);
                            ChatClient.protectedSetIfAccept(0);
                            ChatClient.protectedStandardOutput("Accept the Invitation? (Y/N)");
                            //Set the P2P client flag to true, enter the P2P mode
                            ChatClient.protectedSetClientFlag(true);

                            while (ChatClient.readIfAccept() == 0) {
                                TimeUnit.SECONDS.sleep(1);//Release Pressure of waiting for confirmation.
                            }
                            if (ChatClient.readIfAccept() == 1) {
                                partnerSocket = ChatClient.P2PSocketAsClient;
                                P2Pin = new BufferedReader(new InputStreamReader(partnerSocket.getInputStream()));
                                while (ChatClient.readClientFlag()) { //P2P client receives massage here
                                    message = P2Pin.readLine();
                                    if (message != null) {
                                        if (message.equals("#BACK")) {//If received a "BACK" from P2Pserver, give back the "BACK" to P2Pserver and this will turnoff the P2P mode of both side.
                                            PrintWriter P2Pout = new PrintWriter(ChatClient.P2PSocketAsClient.getOutputStream(), true);
                                            P2Pout.println("#BACK");
                                            ChatClient.protectedStandardOutput("System Info: P2P Chat Ended. Back to Chatting Room");
                                            ChatClient.protectedSetClientFlag(false);//The key of turning off P2P is using flag status to let main thread know what happened here.

                                        } else {
                                            ChatClient.protectedStandardOutput(message);
                                        }
                                    }
                                }
                            } else if (ChatClient.readIfAccept() == -1){

                            }
                        } else if (message.equals("###BADCOMMAND###")) {
                            ChatClient.protectedStandardOutput("Incorrect command. Try again.");
                        } else {
                            ChatClient.protectedStandardOutput(message);
                        }
                    }
                }
            }
            finally {
                serverSocket.close();
            }
        }
        catch (Exception e) {
            System.out.println(e);
        }

    }

}
