
import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by zhangkaibing on 3/12/15.
 */
public class ChatClient {
    public static String Username;
    public static Socket P2PSocketAsServer;
    public static Socket P2PSocketAsClient;
    public static String P2PIP;
    public static int P2PportNumber;
    private static boolean P2PClientFlag;
    private static boolean P2PServerFlag;
    private static int IfAcceptP2P = 0;      //track client's status
    private static ReentrantLock standardOutputLocker = new ReentrantLock();
    private static ReentrantLock clientFlagLocker = new ReentrantLock();
    private static ReentrantLock serverFlagLocker = new ReentrantLock();
    private static ReentrantLock ifAcceptLocker = new ReentrantLock();

    public static void main(String[] args) throws IOException{
        if (args.length != 2) {
            System.err.println("Usage: java ChatClient <host name> <port number>.");
            System.exit(1);
        }

        String hostName = args[0];
        int portNumber = Integer.parseInt(args[1]);

        try {
            protectedStandardOutput("HELLO! Welcome to Kaibing's Chatting System");
            Socket socket = new Socket(hostName, portNumber);
            PrintWriter out = new PrintWriter(socket.getOutputStream(), true);

            //Thread:  Receive Msg from server
            ClientScanMsg clientscan=new ClientScanMsg(socket);
            Thread scanMsgFromServer=new Thread(clientscan);

            //Thread:  Heartbeat that send the "LIVE" signal every 5 secs
            ClientHeartBeat hb=new ClientHeartBeat(socket);
            Thread sendHeatbeatToServer=new Thread(hb);

            //Verification
            ClientVerify clientverify=new ClientVerify(socket);
            boolean Verified=clientverify.Verified();
            if (Verified) {//Start Two Threads
                Username=clientverify.Username;
                scanMsgFromServer.start();
                sendHeatbeatToServer.start();
            }

            Scanner sca = new Scanner(System.in);
            String InputStream = "";
            while(Verified) {
                //System.out.println(Username+":");
                //Scanner sca = new Scanner(System.in);
                InputStream = sca.nextLine();
                if (readClientFlag()){
                    //InputStream = sca.nextLine();
                    while (true) {      //Reset IfAcceptP2P
                        if (InputStream.equals("Y")){
                            protectedSetIfAccept(1);
                            break;
                        } else if (InputStream.equals("N")){
                            protectedSetIfAccept(-1);
                            break;
                        } else {
                            protectedStandardOutput("Please input Y or N");
                            InputStream = sca.nextLine();
                        }
                    }
                    if (readIfAccept() == 1) {   //P2P client
                        protectedStandardOutput("Accepted P2P invitation!");
                        P2PSocketAsClient = new Socket(P2PIP, P2PportNumber);
                        //if (P2Psocket != null)
                        PrintWriter P2Pout = new PrintWriter(P2PSocketAsClient.getOutputStream(), true);
                        P2Pout.println(Username + ":(P2P) " + "I accepted your invitation to connect");
                        while (true) {
                            InputStream = sca.nextLine();
                            if (readClientFlag()) {
                                if (InputStream.equals("#BACK")) {
                                    P2Pout.println("#BACK");
                                    protectedStandardOutput("System Info: P2P Chat Ended. Back to Chatting Room");
                                    protectedSetClientFlag(false);
                                    //IfAcceptP2P = 0;
                                    //break;
                                } else {
                                    P2Pout.println(Username + ":(P2P) " + InputStream);     //client sends P2P message
                                }
                            } else {
                                out.println(InputStream);
                                break;
                            }

                        }
                    } else if (readIfAccept() == -1) {
                        protectedStandardOutput("Declined P2P invitation!");
                        P2PSocketAsClient = new Socket(P2PIP, P2PportNumber);
                        PrintWriter P2Pout = new PrintWriter(P2PSocketAsClient.getOutputStream(), true);
                        P2Pout.println("#DECLINE");
                        protectedSetClientFlag(false);
                    }
                } else if (readServerFlag()) {
                    PrintWriter P2Pout = new PrintWriter(P2PSocketAsServer.getOutputStream(), true);
                    while(true) {

                        if (InputStream.equals("#BACK")) {
                            P2Pout.println("#BACK");        //tell P2P client it wants back
                            protectedSetServerFlag(false);
                            break;
                        }
                        if (readServerFlag()) {
                            P2Pout.println(Username + ":(P2P) " + InputStream);
                        } else {
                            out.println(InputStream);
                            break;
                        }
                        InputStream = sca.nextLine();       //blocked here
                    }
                } else if (InputStream.equals("#P2P")) {
                    out.println("###P2P###");
                }
                /*
                else if (InputStream.equals("#BLACK")) {
                    out.println("###BLACK###");
                } else if (InputStream.equals("#UNBLACK")){
                    out.println("###UNBLACK###");
                }
                */
                else if (InputStream.equals("logout")) {
                    out.println("###LOGOUT###");
                    System.exit(0);
                } else if (InputStream.equals("online")) {
                    out.println("###ONLINE###");
                } else {
                    out.println(InputStream);
                }
            }
        } catch (IOException e) {
            System.err.println("Couldn't get I/O for the connection to " + hostName);
            System.exit(1);
        }
    }

    public static void requestP2P(int portNumber) {     //inviting partner be the server
        try {
            //Setup server and waiting for partner connection
            ServerSocket serverSocket = new ServerSocket(portNumber);
            protectedStandardOutput("System Info: Waiting for Partner Connection...");
            //partner connected
            P2PSocketAsServer = serverSocket.accept();
            protectedStandardOutput("Accepted port:" + P2PSocketAsServer.getPort());
            protectedSetServerFlag(true);
            protectedStandardOutput("Invitation Accepted! ");
            //Partner connected, setup thread for listening to partner.
            ClientP2P P2Pchat = new ClientP2P(P2PSocketAsServer);
            Thread t_P2Pchat = new Thread(P2Pchat);
            t_P2Pchat.start();

        } catch (Exception e) {
            System.out.println(e);
        }
    }

    //System output with protection by reentrantlock.
    public static void protectedStandardOutput(String s) {
        standardOutputLocker.lock();
        System.out.println(s);
        standardOutputLocker.unlock();
    }
    //There are two threads that can change the value of P2PClientFlag, so we need a locker to protect the value
    public static void protectedSetClientFlag(Boolean b) {
        clientFlagLocker.lock();
        P2PClientFlag = b;
        clientFlagLocker.unlock();
    }
    //There are two threads that can change the value of P2PServerFlag, so we need a locker to protect the value
    public static void protectedSetServerFlag(Boolean b) {
        serverFlagLocker.lock();
        P2PServerFlag = b;
        serverFlagLocker.unlock();
    }
    //There are two threads that can change the value of IfAcceptP2P, so we need a locker to protect the value
    public static void protectedSetIfAccept(Integer i) {
        ifAcceptLocker.lock();
        IfAcceptP2P = i;
        ifAcceptLocker.unlock();
    }

    public static boolean readClientFlag() {
        return P2PClientFlag;
    }

    public static boolean readServerFlag() {
        return P2PServerFlag;
    }

    public static int readIfAccept() {
        return IfAcceptP2P;
    }
}
