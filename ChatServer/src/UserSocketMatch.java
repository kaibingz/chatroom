import java.net.Socket;

/**
 * Created by zhangkaibing on 3/13/15.
 */
public class UserSocketMatch {
    private String username;
    private Socket userSocket;

    public UserSocketMatch(String un, Socket us) {
        username = un;
        userSocket = us;
    }

    public Socket getUserSocket() {
        return userSocket;
    }

    public String getUsername() {
        return username;
    }
}
