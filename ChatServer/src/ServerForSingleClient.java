import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.*;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by zhangkaibing on 3/10/15.
 */
public class ServerForSingleClient implements Runnable {

    private Socket clientSocket;
    private PrintWriter out;
    private BufferedReader in;
    private String message = "";

    private ReentrantLock heartbeatLocker = new ReentrantLock();
    private String username = "";
    private String password = "";
    private String requiredPwd;

    public ServerForSingleClient(Socket s) {
        clientSocket = s;
    }

    public void run() {
        try {
            try {
                out = new PrintWriter(clientSocket.getOutputStream(), true);
                in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

                pushMessage(out, "###CONNECTED###");
                int flag = -1;
                //String lastUsername = "";

                while (true) {
                    pushMessage(out, "###USERNAME###");
                    username = in.readLine();
                    ChatServer.protectedStandardOutput("Username: " + username);
                    if (!ChatServer.userCredentials.containsKey(username)) {
                        ChatServer.protectedStandardOutput("Username doesn't exist.");
                        pushMessage(out, "###NOTEXIST###");
                        continue;
                    } else {
                        if ((System.currentTimeMillis() - ChatServer.getAuthenticationTime(username)) < 60 * 1000) {
                            ChatServer.protectedStandardOutput("[Entered this loop.]");
                            pushMessage(out, "###BLOCKED###");
                            break;
                        } else {

                            for (int i = 1; i <= 3; i++) {
                                pushMessage(out, "###PASSWORD###");
                                password = in.readLine();
                                ChatServer.protectedStandardOutput("Password: " + password);
                                requiredPwd = ChatServer.userCredentials.get(username);
                                if (!requiredPwd.equals(password)) {
                                    pushMessage(out, "###PWDNOTMATCH###");
                                    continue;
                                } else {
                                    ChatServer.connectedClientsLocker.lock();
                                    ChatServer.connectedClients.put(username, clientSocket);
                                    ChatServer.connectedClientsLocker.unlock();
                                    pushMessage(out, "###SUCCEED###");
                                    ChatServer.protectedStandardOutput("Connected from: " + clientSocket.getLocalAddress().getHostName());
                                    flag = 0;
                                    ChatServer.clearAuthenticationBlocker(username);
                                    break;
                                }

                            }
                            if (flag == -1) {
                                pushMessage(out, "###BLOCKED###");
                                ChatServer.setAuthenticationBlocker(username);
                                clientSocket.close();
                                return;
                            } else {
                                break;
                            }
                        }
                    }
                }

                //User Login Broadcasting
                Iterator Iter = ChatServer.connectedClients.entrySet().iterator();
                while (Iter.hasNext()) {
                    Map.Entry entry = (Map.Entry) Iter.next();
                    String tmpName = (String) entry.getKey();
                    if (!tmpName.equals(username)) {
                        if (!ChatServer.blackList.get(username).contains(tmpName)) {
                            Socket tmpSocket = ChatServer.connectedClients.get(tmpName);
                            PrintWriter tmpOut = new PrintWriter(tmpSocket.getOutputStream(), true);
                            pushMessage(tmpOut, "Broadcast: " + username + " logged in.");
                        }
                    }
                }

                LinkedList<String> offlineMessage = ChatServer.offlineChatRecord.get(username);
                while (!offlineMessage.isEmpty()) {
                    pushMessage(out, offlineMessage.remove());
                }

                ConnectionCheck cc = new ConnectionCheck(heartbeatLocker, username);
                Thread connectionCheck = new Thread(cc);
                connectionCheck.start();

                while (true) {

                    if (!clientSocket.isConnected()) {
                        ChatServer.connectedClientsLocker.lock();
                        ChatServer.connectedClients.remove(username);
                        ChatServer.connectedClientsLocker.unlock();
                        break;
                    }
                    //Implementing various functionality
                    if ((message = in.readLine()) != null) {
                        if (message.equals("###HEARTBEAT###")) {//Heartbeat
                            heartbeatLocker.lock();
                            cc.ResetHeartbeat();
                            heartbeatLocker.unlock();
                        } else if (message.equals("###P2P###")) {
                            String pUserName;
                            ChatServer.protectedStandardOutput("P2P Request!!!!!!!");
                            pushMessage(out, "###PARTNER###");
                            while (true) {
                                pUserName = in.readLine();      //Because here we kidnapped the input stream, so we have to take care of the heart beat here
                                if (pUserName.equals("###HEARTBEAT###")) {
                                    heartbeatLocker.lock();
                                    cc.ResetHeartbeat();
                                    heartbeatLocker.unlock();
                                } else break;
                            }
                            if (ChatServer.connectedClients.containsKey(pUserName)) {
                                LinkedList<String> blacklist = ChatServer.blackList.get(pUserName);
                                if (blacklist.contains(username))//If some client want to setup P2P, finally check if its in other's black list.
                                    pushMessage(out, "###BLOCKED###");
                                else {
                                    //Push command and a random port number to inviting partner
                                    Random random = new Random();
                                    int portNumber = random.nextInt(3977) + 1024;
                                    pushMessage(out, "###P2PSETUP###" + portNumber);
                                    //push message to invited partner
                                    Socket tmpSocket = ChatServer.connectedClients.get(pUserName);
                                    PrintWriter tmpOut = new PrintWriter(tmpSocket.getOutputStream(), true);
                                    pushMessage(tmpOut, "###P2PINVITE###" + username + "###" + clientSocket.getLocalAddress().getHostAddress() + "###" + portNumber);
                                }
                            } else if (ChatServer.userCredentials.containsKey(pUserName)) {
                                pushMessage(out, "###OFFLINE###");
                            } else {
                                pushMessage(out, "###NOTEXIST###");
                            }
                        } else if (message.length() >= 5 && message.substring(0,5).equals("block")) {//BLACK Listing
                            ChatServer.protectedStandardOutput("Block request.");
                            String pUserName = message.substring(6);
                            /*
                            pushMessage(out, "###BLACKWHOM###");
                            while (true) {
                                pUserName = in.readLine();      //Because here we kidnapped the input stream, so we have to take care of the heart beat here
                                if (pUserName.equals("###HEARTBEAT###")) {
                                    heartbeatLocker.lock();
                                    cc.ResetHeartbeat();
                                    heartbeatLocker.unlock();
                                } else break;
                            }
                            */
                            if (ChatServer.userCredentials.containsKey(pUserName)) {
                                LinkedList<String> blacklist = ChatServer.blackList.get(username);
                                blacklist.addLast(pUserName);
                                ChatServer.blackList.put(username, blacklist);
                                pushMessage(out, "###BLOCKSUCCEED###");
                            } else {
                                pushMessage(out, "###NOTEXIST###");
                            }
                        } else if (message.length() >= 7 && message.substring(0,7).equals("unblock")) {
                            ChatServer.protectedStandardOutput("Unblock request.");
                            String pUserName = message.substring(8);
                            /*
                            pushMessage(out, "###UNBLACKWHOM###");
                            while (true) {
                                pUserName = in.readLine();      //Because here we kidnapped the input stream, so we have to take care of the heart beat here
                                if (pUserName.equals("###HEARTBEAT###")) {
                                    heartbeatLocker.lock();
                                    cc.ResetHeartbeat();
                                    heartbeatLocker.unlock();
                                } else break;
                            }
                            */
                            if (ChatServer.userCredentials.containsKey(pUserName)) {
                                LinkedList<String> blacklist = ChatServer.blackList.get(username);
                                blacklist.remove(pUserName);
                                ChatServer.blackList.put(username, blacklist);
                                pushMessage(out, "###UNBLOCKSUCCEED###");
                            } else {
                                pushMessage(out, "###NOTEXIST###");
                            }
                        } else if (message.equals("###LOGOUT###")) {
                            ChatServer.protectedStandardOutput("Logout request.");
                            Iterator iter = ChatServer.offlineChatRecord.entrySet().iterator();
                            while (iter.hasNext()) {
                                Map.Entry entry = (Map.Entry) iter.next();
                                String tmpName = (String) entry.getKey();
                                if (ChatServer.connectedClients.containsKey(tmpName)) {
                                    if (!tmpName.equals(username)) {
                                        if (!ChatServer.blackList.get(username).contains(tmpName)) {
                                            Socket tmpSocket = ChatServer.connectedClients.get(tmpName);
                                            PrintWriter tmpOut = new PrintWriter(tmpSocket.getOutputStream(), true);
                                            pushMessage(tmpOut, "Broadcast: " + username + " logged out.");//Log out broadcasting
                                        }
                                    }
                                }
                            }
                            ChatServer.connectedClients.remove(username);
                            break;
                        } else if (message.equals("###ONLINE###")) {
                            String onlineUsers = "";
                            Iterator iter = ChatServer.connectedClients.entrySet().iterator();
                            while (iter.hasNext()) {
                                Map.Entry entry = (Map.Entry) iter.next();
                                onlineUsers = onlineUsers + (String)entry.getKey() + " ";
                            }
                            pushMessage(out, onlineUsers);
                        }

                        else if (message.length() >= 7 && message.substring(0,7).equals("message")) {
                            StringTokenizer tokenizer = new StringTokenizer(message);
                            tokenizer.nextToken();
                            String toWhom = tokenizer.nextToken();
                            String what = tokenizer.nextToken();
                            if (ChatServer.blackList.get(toWhom).contains(username)) {
                                pushMessage(out, "###BLOCKED###");
                            } else {
                                if (ChatServer.connectedClients.containsKey(toWhom)) {
                                    Socket tmpSocket = ChatServer.connectedClients.get(toWhom);
                                    PrintWriter tmpOut = new PrintWriter(tmpSocket.getOutputStream(), true);
                                    pushMessage(tmpOut, username + ": " + what);
                                    ChatServer.protectedStandardOutput("Sent to " + toWhom);
                                } else {
                                    ChatServer.offlineRecordLocker.lock();
                                    LinkedList<String> records = ChatServer.offlineChatRecord.get(toWhom);
                                    records.addLast(username + ": " + what);
                                    ChatServer.offlineChatRecord.put(toWhom, records);
                                    ChatServer.offlineRecordLocker.unlock();
                                    ChatServer.protectedStandardOutput("Put offline message to " + toWhom);
                                }
                            }
                        } else if (message.length() >= 9 && message.substring(0,9).equals("broadcast")) {
                            //Now it is the only way to send broadcasts.
                            message = message.substring(10);
                            ChatServer.protectedStandardOutput(clientSocket.getLocalAddress().getHostName() + " said: " + message);
                            Iterator iter = ChatServer.offlineChatRecord.entrySet().iterator();
                            while (iter.hasNext()) {
                                Map.Entry entry = (Map.Entry) iter.next();
                                String tmpName = (String) entry.getKey();
                                //Push message to online users
                                if (!ChatServer.blackList.get(tmpName).contains(username)) {
                                    if (ChatServer.connectedClients.containsKey(tmpName)) {
                                        if (!tmpName.equals(username)) {
                                            Socket tmpSocket = ChatServer.connectedClients.get(tmpName);
                                            PrintWriter tmpOut = new PrintWriter(tmpSocket.getOutputStream(), true);
                                            pushMessage(tmpOut, username + ": " + message);
                                            ChatServer.protectedStandardOutput("Sent to " + tmpName);
                                        }
                                    } else {
                                        //Push message to offline users
                                        ChatServer.offlineRecordLocker.lock();
                                        LinkedList<String> records = ChatServer.offlineChatRecord.get(tmpName);
                                        records.addLast(username + ": " + message);
                                        ChatServer.offlineChatRecord.put(tmpName, records);
                                        ChatServer.offlineRecordLocker.unlock();
                                        ChatServer.protectedStandardOutput("Put offline message to " + tmpName);
                                    }
                                }
                            }
                        } else {
                            pushMessage(out, "###BADCOMMAND###");
                        }
                    }
                }
            } finally {
                clientSocket.close();
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    private void pushMessage(PrintWriter o, String s) {
        ChatServer.streamLocker.lock();
        o.println(s);
        ChatServer.streamLocker.unlock();
    }

}