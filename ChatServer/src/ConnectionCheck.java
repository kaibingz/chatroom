import java.io.IOException;
import java.net.Socket;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by zhangkaibing on 3/12/15.
 */
//Connection check, if haven't receive heartbeat from user for 50seconds, disconnect the user.
public class ConnectionCheck implements Runnable {

    private Integer heartbeat;
    private ReentrantLock heartbeatLocker;
    private String client;

    public ConnectionCheck( ReentrantLock hl, String user) {
        heartbeat = 0;
        heartbeatLocker = hl;
        client = user;
    }
    public void ResetHeartbeat(){
        heartbeat=0;
    }
    public void run() {
        try {
            while (true) {

                TimeUnit.SECONDS.sleep(5);
                heartbeatLocker.lock();
                heartbeat++;
                heartbeatLocker.unlock();
                if (heartbeat >= 6) {
                    Socket clientSocket = ChatServer.connectedClients.get(client);
                    clientSocket.close();
                    ChatServer.connectedClients.remove(client);
                    break;
                }
            }
        } catch (InterruptedException e) {
            System.out.println("sleep() interrupted.");
        } catch (IOException e) {
            System.out.println("Unable to close socket.");
        }
    }


}
