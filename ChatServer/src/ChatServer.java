/**
 * Created by zhangkaibing on 3/10/15.
 */


import java.io.File;
import java.io.FileNotFoundException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.*;
import java.util.concurrent.locks.ReentrantLock;

public class ChatServer {

    public static HashMap<String, String> userCredentials = new HashMap<String, String>();
    public static HashMap<String, Socket> connectedClients = new HashMap<String, Socket>();
    public static HashMap<String, LinkedList<String>> offlineChatRecord = new HashMap<String, LinkedList<String>>();
    public static HashMap<String, LinkedList<String>> blackList = new HashMap<String, LinkedList<String>>();
    public static final ReentrantLock connectedClientsLocker = new ReentrantLock();
    public static final ReentrantLock streamLocker = new ReentrantLock();
    public static final ReentrantLock offlineRecordLocker = new ReentrantLock();
    private static ReentrantLock standardOutputLocker = new ReentrantLock();
    private static HashMap<String, Long> authenticationBlocker = new HashMap<String, Long>();

    public static void main(String[] args) {
        if (args.length != 1) {
            System.err.println("Usage: java ChatServer <port number>");
        }

        File credentialsFile = new File("credentials.txt");
        try {
            Scanner credentialsScan = new Scanner(credentialsFile);
            while (credentialsScan.hasNext()) {
                userCredentials.put(credentialsScan.next(), credentialsScan.next());
            }
        } catch (FileNotFoundException e) {
            System.out.println("Unable to find credentials file.");
        }

        /*
        Iterator iter = userCredentials.entrySet().iterator();
        while (iter.hasNext()) {
            Map.Entry entry = (Map.Entry) iter.next();
            System.out.println(entry.getKey());
            System.out.println(entry.getValue());
        }
        */

        Iterator iter = userCredentials.entrySet().iterator();
        while (iter.hasNext()) {
            Map.Entry entry = (Map.Entry) iter.next();
            String username = (String)entry.getKey();
            offlineChatRecord.put(username, new LinkedList<String>());
            blackList.put(username, new LinkedList<String>());
            authenticationBlocker.put(username, (long)0);
        }

        try {
            ServerSocket serverSocket = new ServerSocket(Integer.parseInt(args[0]));
            protectedStandardOutput("Waiting for connection...");

            while (true) {
                Socket clientSocket = serverSocket.accept();
                protectedStandardOutput("Request from: " + clientSocket.getLocalAddress().getHostName());

                ServerForSingleClient chat = new ServerForSingleClient(clientSocket);
                Thread thread = new Thread(chat);
                thread.start();
            }
        } catch (Exception e) {
            System.out.println(e);
        }

    }

    public static void protectedStandardOutput(String s) {
        standardOutputLocker.lock();
        System.out.println(s);
        standardOutputLocker.unlock();
    }

    public static void setAuthenticationBlocker(String user) {
        authenticationBlocker.put(user, System.currentTimeMillis());
    }
    public static void clearAuthenticationBlocker(String user) {
        authenticationBlocker.put(user, (long)0);
    }
    public static Long getAuthenticationTime(String user) {
        return authenticationBlocker.get(user);
    }
}
